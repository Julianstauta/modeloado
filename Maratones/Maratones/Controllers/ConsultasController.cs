﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Maratones.Data;
using Maratones.Models;

namespace Maratones.Controllers
{
    public class ConsultasController : Controller
    {
        private readonly ApplicationDbContext _ctx;
        public ConsultasController(ApplicationDbContext context)
        {
            _ctx = context;
        }

        //1. cuántos equipos tiene cada profesor. 

        public class equipoProfesor
        {
            public string Profesor { get; set; }
            public string Cedula { get; set; }
            public int Equipos { get; set; }
        }

        public ActionResult ListaProfesores()
        {
            var listas = _ctx.Profesor.Select(e => new equipoProfesor
            {
                Profesor = e.Nombre,
                Cedula = e.Cedula,
                Equipos = e.Equipo.Count()
            });
            return View(listas);
        }

        //2. El numero de participantes en una maraton

        public class participantes
        {
            public string Nombre { get; set; }
            public int NumParticipaciones { get; set; }
        }

        public ActionResult participantesMaraton()
        {
            Func<Maraton, int> coach = x => x.Participacion.Select(p => p.Equipo.ProfesorCedula).Distinct().Count();
            var numParticipantes = _ctx.Maraton.Select(e => new participantes { Nombre = e.NombreMaraton, NumParticipaciones = e.Participacion.Count() * 3 + coach(e) });
            return View(numParticipantes);
        }

        //3. Los premios ganados por un equipo

        public class premios
        {
            public string Nombre { get; set; }
            public IEnumerable<int> Premios { get; set; }
        }

        public ActionResult premiosEquipo()
        {
            var premios = _ctx.Equipo.Select(c => new premios
            {
                Nombre = c.Nombre,
                Premios = c.Participacion.Where(e => e.Puesto == 1).Select(y => y.Puesto)
            });
            return View(premios);
        }

        //4. equipos que ganaron una maraton patrocinada por una organizacon
        public ActionResult ganadoresPatrocinada(String patrocinio)
        {
            var maratones = _ctx.Maraton.Select(e => e.Contrato.Where(t => t.Patrocinador.Nombre.Equals(patrocinio)).Select(t => t.Maraton.Ganador));
            return View(maratones);
        }

        //5. estudiantes que mas ejercicios han resuelto en una universidad
        public ActionResult estudiantesMasEjercicios(string universidad)
        {
            var estudiantes = _ctx.Universidad.Where(e => e.Nombre.Equals(universidad)).First().Estudiante.OrderByDescending(e => e.Equipo.Participacion.Sum(s => s.NumeroEjerciciosResueltos));
            return View(estudiantes);
        }

        //6. numero ejercicios resueltos por equipo
        public class resueltosPorEquipo
        {
            public String nombre { get; set; }
            public int numeroEjercicios { get; set; }
        }
        public ActionResult ejerciciosResueltos()
        {
            var ejercisios = _ctx.Equipo.Select(c => new resueltosPorEquipo { nombre = c.Nombre, numeroEjercicios = c.Participacion.Sum(s => s.NumeroEjerciciosResueltos) });
            return View(ejercisios);
        }


        //7. el profesor con el mejor promedio de puestos en sus equipos
        public class profe
        {
            public string nombre { get; set; }
            public string cedula { get; set; }
            public double promedio { get; set; }
        }
        public ActionResult mejorPromedioProfesor()
        {
            var profesor = _ctx.Profesor.Select(c => new profe { nombre = c.Nombre, cedula = c.Cedula, promedio = c.Equipo.Average(a => a.Participacion.Average(l => l.Puesto)) }).OrderByDescending(t => t.promedio).First();
            return View(profesor);
        }

        //8. premios ganados por un equipo
        public class premiosEquipos
        {
            public string nombre { get; set; }
            public IEnumerable<string> premios { get; set; }
        }
        public ActionResult ganadorPorEquipo()
        {
            var premiosPorEquipo = _ctx.Equipo.Select(c => new premiosEquipos { nombre = c.Nombre, premios = c.Participacion.Where(p => p.Puesto == 1).Select(i => i.Maraton.Premio) });
            return View(premiosPorEquipo);
        }
        //9. el problema que mas aparecio
        public ActionResult problemaMasAparecio()
        {
            var problema = _ctx.EjercicioMaraton.GroupBy(x => x.EjercicioId).OrderBy(n => n.Count());
            return View(problema.First());

        }
        //10. universidad con mas ganadores
        public ActionResult universidadGanadores()
        {
            var masGanador = _ctx.Maraton.Select(x => x.Ganador).GroupBy(n => n).OrderBy(n => n.Count()).First();
            var univ = _ctx.Equipo.Where(x => x.Nombre.Equals(masGanador)).First().Profesor.Universidad.Nombre;
            return View(univ);
        }

        public IActionResult Index()
        {
            return View();
        }
    }

}