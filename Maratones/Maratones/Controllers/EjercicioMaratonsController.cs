﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Maratones.Data;
using Maratones.Models;

namespace Maratones.Controllers
{
    public class EjercicioMaratonsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EjercicioMaratonsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EjercicioMaratons
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.EjercicioMaraton.Include(e => e.Ejercicio).Include(e => e.Maraton);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: EjercicioMaratons/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ejercicioMaraton = await _context.EjercicioMaraton
                .Include(e => e.Ejercicio)
                .Include(e => e.Maraton)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ejercicioMaraton == null)
            {
                return NotFound();
            }

            return View(ejercicioMaraton);
        }

        // GET: EjercicioMaratons/Create
        public IActionResult Create()
        {
            ViewData["EjercicioId"] = new SelectList(_context.Set<Ejercicio>(), "Id", "Nombre");
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton");
            return View();
        }

        // POST: EjercicioMaratons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,MaratonId,EjercicioId")] EjercicioMaraton ejercicioMaraton)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ejercicioMaraton);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EjercicioId"] = new SelectList(_context.Set<Ejercicio>(), "Id", "Nombre", ejercicioMaraton.EjercicioId);
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton", ejercicioMaraton.MaratonId);
            return View(ejercicioMaraton);
        }

        // GET: EjercicioMaratons/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ejercicioMaraton = await _context.EjercicioMaraton.SingleOrDefaultAsync(m => m.Id == id);
            if (ejercicioMaraton == null)
            {
                return NotFound();
            }
            ViewData["EjercicioId"] = new SelectList(_context.Set<Ejercicio>(), "Id", "Nombre", ejercicioMaraton.EjercicioId);
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton", ejercicioMaraton.MaratonId);
            return View(ejercicioMaraton);
        }

        // POST: EjercicioMaratons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,MaratonId,EjercicioId")] EjercicioMaraton ejercicioMaraton)
        {
            if (id != ejercicioMaraton.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ejercicioMaraton);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EjercicioMaratonExists(ejercicioMaraton.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EjercicioId"] = new SelectList(_context.Set<Ejercicio>(), "Id", "Nombre", ejercicioMaraton.EjercicioId);
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton", ejercicioMaraton.MaratonId);
            return View(ejercicioMaraton);
        }

        // GET: EjercicioMaratons/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ejercicioMaraton = await _context.EjercicioMaraton
                .Include(e => e.Ejercicio)
                .Include(e => e.Maraton)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ejercicioMaraton == null)
            {
                return NotFound();
            }

            return View(ejercicioMaraton);
        }

        // POST: EjercicioMaratons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ejercicioMaraton = await _context.EjercicioMaraton.SingleOrDefaultAsync(m => m.Id == id);
            _context.EjercicioMaraton.Remove(ejercicioMaraton);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EjercicioMaratonExists(int id)
        {
            return _context.EjercicioMaraton.Any(e => e.Id == id);
        }
    }
}
