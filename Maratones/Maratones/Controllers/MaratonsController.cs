﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Maratones.Data;
using Maratones.Models;

namespace Maratones.Controllers
{
    public class MaratonsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MaratonsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Maratons
        public async Task<IActionResult> Index()
        {
            return View(await _context.Maraton.ToListAsync());
        }

        // GET: Maratons/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var maraton = await _context.Maraton
                .SingleOrDefaultAsync(m => m.Id == id);
            if (maraton == null)
            {
                return NotFound();
            }

            return View(maraton);
        }

        // GET: Maratons/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Maratons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NombreMaraton,Id,Ganador,Premio")] Maraton maraton)
        {
            if (ModelState.IsValid)
            {
                _context.Add(maraton);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(maraton);
        }

        // GET: Maratons/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var maraton = await _context.Maraton.SingleOrDefaultAsync(m => m.Id == id);
            if (maraton == null)
            {
                return NotFound();
            }
            return View(maraton);
        }

        // POST: Maratons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("NombreMaraton,Id,Ganador,Premio")] Maraton maraton)
        {
            if (id != maraton.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(maraton);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MaratonExists(maraton.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(maraton);
        }

        // GET: Maratons/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var maraton = await _context.Maraton
                .SingleOrDefaultAsync(m => m.Id == id);
            if (maraton == null)
            {
                return NotFound();
            }

            return View(maraton);
        }

        // POST: Maratons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var maraton = await _context.Maraton.SingleOrDefaultAsync(m => m.Id == id);
            _context.Maraton.Remove(maraton);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MaratonExists(int id)
        {
            return _context.Maraton.Any(e => e.Id == id);
        }
    }
}
