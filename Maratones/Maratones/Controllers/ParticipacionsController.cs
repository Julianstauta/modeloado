﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Maratones.Data;
using Maratones.Models;

namespace Maratones.Controllers
{
    public class ParticipacionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ParticipacionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Participacions
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Participacion.Include(p => p.Equipo).Include(p => p.Maraton);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Participacions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participacion = await _context.Participacion
                .Include(p => p.Equipo)
                .Include(p => p.Maraton)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (participacion == null)
            {
                return NotFound();
            }

            return View(participacion);
        }

        // GET: Participacions/Create
        public IActionResult Create()
        {
            ViewData["EquipoId"] = new SelectList(_context.Equipo, "Id", "Nombre");
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton");
            return View();
        }

        // POST: Participacions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Puesto,NumeroEjerciciosResueltos,Id,EquipoId,MaratonId")] Participacion participacion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(participacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipoId"] = new SelectList(_context.Equipo, "Id", "Nombre", participacion.EquipoId);
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton", participacion.MaratonId);
            return View(participacion);
        }

        // GET: Participacions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participacion = await _context.Participacion.SingleOrDefaultAsync(m => m.Id == id);
            if (participacion == null)
            {
                return NotFound();
            }
            ViewData["EquipoId"] = new SelectList(_context.Equipo, "Id", "Nombre", participacion.EquipoId);
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton", participacion.MaratonId);
            return View(participacion);
        }

        // POST: Participacions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Puesto,NumeroEjerciciosResueltos,Id,EquipoId,MaratonId")] Participacion participacion)
        {
            if (id != participacion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(participacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ParticipacionExists(participacion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipoId"] = new SelectList(_context.Equipo, "Id", "Nombre", participacion.EquipoId);
            ViewData["MaratonId"] = new SelectList(_context.Maraton, "Id", "NombreMaraton", participacion.MaratonId);
            return View(participacion);
        }

        // GET: Participacions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participacion = await _context.Participacion
                .Include(p => p.Equipo)
                .Include(p => p.Maraton)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (participacion == null)
            {
                return NotFound();
            }

            return View(participacion);
        }

        // POST: Participacions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var participacion = await _context.Participacion.SingleOrDefaultAsync(m => m.Id == id);
            _context.Participacion.Remove(participacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ParticipacionExists(int id)
        {
            return _context.Participacion.Any(e => e.Id == id);
        }
    }
}
