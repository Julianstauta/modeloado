﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Maratones.Models;

namespace Maratones.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Contrato>().HasAlternateKey(c => new { c.MaratonId , c.PatrocinadorId});
            builder.Entity<EjercicioMaraton>().HasAlternateKey(e => new { e.MaratonId, e.EjercicioId });
            builder.Entity<Participacion>().HasAlternateKey(p => new { p.EquipoId, p.MaratonId });
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Maratones.Models.Equipo> Equipo { get; set; }

        public DbSet<Maratones.Models.Maraton> Maraton { get; set; }

        public DbSet<Maratones.Models.Estudiante> Estudiante { get; set; }

        public DbSet<Maratones.Models.Profesor> Profesor { get; set; }

        public DbSet<Maratones.Models.Universidad> Universidad { get; set; }

        public DbSet<Maratones.Models.Participacion> Participacion { get; set; }

        public DbSet<Maratones.Models.EjercicioMaraton> EjercicioMaraton { get; set; }

        public DbSet<Maratones.Models.Ejercicio> Ejercicio { get; set; }

        public DbSet<Maratones.Models.Contrato> Contrato { get; set; }

        public DbSet<Maratones.Models.Patrocinador> Patrocinador { get; set; }
    }
}
