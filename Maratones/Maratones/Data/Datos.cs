﻿using Maratones.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Maratones.Data
{
    public class Datos
    {
        public static void Cargar(IServiceProvider serviceProvider)
        {
            using (var ctx = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                if (ctx.Estudiante.Any())
                {
                    return;   // La base de datos ya tiene datos               
                }
                var profesores = new List<Profesor>{
                    new Profesor { Nombre = "Guillermo Londoño", Cedula = "30302878"},
                    new Profesor { Nombre = "Juan Manuel Reyes", Cedula = "16320529"},
                    new Profesor { Nombre = "Nora M. Villegas", Cedula = "36330315"},
                    new Profesor { Nombre = "Luis Eduardo Munera", Cedula = "16130828"},
                    new Profesor { Nombre = "Gabriel Tamura", Cedula = "50625935"}
                };
                profesores.ForEach(e => ctx.Profesor.Add(e));
                ctx.SaveChanges();

                var universidades = new List<Universidad>{
                    new Universidad { Nombre = "ICESI"},
                    new Universidad { Nombre = "Nacional"},
                    new Universidad { Nombre = "Javeriana"},
                    new Universidad { Nombre = "Univalle"}
                };
                universidades.ForEach(e => ctx.Universidad.Add(e));
                ctx.SaveChanges();

                var equipos = new List<Equipo>{
                    new Equipo { Nombre = "AnvilKaputt", ProfesorCedula = "30302878", ProfesorId = 1, UniversidadId = 3},
                    new Equipo { Nombre = "JIM The Ripper", ProfesorCedula = "16320529", ProfesorId = 2, UniversidadId = 1},
                    new Equipo { Nombre = "Super Pollos", ProfesorCedula = "36330315", ProfesorId = 3, UniversidadId = 3},
                    new Equipo { Nombre = "FunSociety", ProfesorCedula = "16130828", ProfesorId = 4, UniversidadId = 2},
                    new Equipo { Nombre = "Build The Wall", ProfesorCedula = "5062593", ProfesorId = 5, UniversidadId = 4},
                    new Equipo { Nombre = "IcesiOldSchool", ProfesorCedula = "30302878", ProfesorId = 1, UniversidadId = 1},
                    new Equipo { Nombre = "Epic", ProfesorCedula = "16320529", ProfesorId = 2, UniversidadId = 4},
                    new Equipo { Nombre = "ITIS", ProfesorCedula = "36330315", ProfesorId = 3, UniversidadId = 1},
                    new Equipo { Nombre = "UNSwifty", ProfesorCedula = "5062593",ProfesorId = 4, UniversidadId = 2}
                };
                equipos.ForEach(e => ctx.Equipo.Add(e));
                ctx.SaveChanges();

                var estudiantes = new List<Estudiante>{
                    new Estudiante { Nombre = "Eric", Cedula = "1633051473099" , CodigoEstudiante = "11337167199"},
                    new Estudiante { Nombre = "Yoshio", Cedula = "1680102459599", CodigoEstudiante = "25332154799"},
                    new Estudiante { Nombre = "Ferdinand", Cedula = "1663092311199", CodigoEstudiante = "92570816299"},
                    new Estudiante { Nombre = "Yuli", Cedula = "1617043013399", CodigoEstudiante = "72609104099"},
                    new Estudiante { Nombre = "Ferdinand", Cedula = "1694062075599", CodigoEstudiante = "34893660399"},
                    new Estudiante { Nombre = "Preston", Cedula = "1651030748499", CodigoEstudiante = "44520527699"},
                    new Estudiante { Nombre = "Eaton", Cedula = "1656031934199", CodigoEstudiante ="01772232799"},
                    new Estudiante { Nombre = "Cedric", Cedula = "1643122535499", CodigoEstudiante = "99600503899"},
                    new Estudiante { Nombre = "Rooney", Cedula = "1600121346599", CodigoEstudiante = "94864122999"},
                    new Estudiante { Nombre = "Allen", Cedula = "1645112923599", CodigoEstudiante = "61906615599"},
                    new Estudiante { Nombre = "Brady", Cedula = "1690050429499", CodigoEstudiante = "28646516999"},
                    new Estudiante { Nombre = "Colorado", Cedula = "1643011904999", CodigoEstudiante = "23827512599"},
                    new Estudiante { Nombre = "Abbot", Cedula = "1617081311499", CodigoEstudiante = "98506658699"},
                    new Estudiante { Nombre = "Abel", Cedula = "1627051441899", CodigoEstudiante = "52430076399"},
                    new Estudiante { Nombre = "Marvin", Cedula = "1629041415799", CodigoEstudiante = "84349775799"},
                    new Estudiante { Nombre = "Francis", Cedula = "1648120651999", CodigoEstudiante = "90696310699"},
                    new Estudiante { Nombre = "Emmanuel", Cedula = "1612112387999", CodigoEstudiante = "08796218899"},
                    new Estudiante { Nombre = "Kaseem", Cedula = "1693021095799", CodigoEstudiante = "59963697099"},
                    new Estudiante { Nombre = "Hilel", Cedula = "1685120136599", CodigoEstudiante = "75526572899"},
                    new Estudiante { Nombre = "Kamal", Cedula = "1695112932699", CodigoEstudiante = "56569270399"},
                    new Estudiante { Nombre = "Tate", Cedula = "1615112874699", CodigoEstudiante = "81503271899"},
                    new Estudiante { Nombre = "Flynn", Cedula = "1631102544499", CodigoEstudiante = "83811877999"},
                    new Estudiante { Nombre = "Baxter", Cedula = "1637112380199", CodigoEstudiante = "78270155799"},
                    new Estudiante { Nombre = "Stewart", Cedula = "1660080920999", CodigoEstudiante = "43925561099"},
                    new Estudiante { Nombre = "Nolan", Cedula = "1634012093099", CodigoEstudiante = "18521291399"},
                    new Estudiante { Nombre = "Davis", Cedula = "1685052050299", CodigoEstudiante = "32255065399"},
                    new Estudiante { Nombre = "Adrian", Cedula = "1698091783299", CodigoEstudiante = "72431550399"}
                };
                estudiantes.ForEach(e => ctx.Estudiante.Add(e));
                ctx.SaveChanges();

                var maratones = new List<Maraton>{
                    new Maraton { Ganador = "AnvilKaputt", Premio = "$500.000", NombreMaraton = "ICPC"},
                    new Maraton { Ganador = "JIM The Ripper", Premio = "$3'000.000", NombreMaraton = "Maraton Nacional 2017"},
                };
                maratones.ForEach(e => ctx.Maraton.Add(e));
                ctx.SaveChanges();

                var participaciones = new List<Participacion>{
                    new Participacion {Puesto = 1,NumeroEjerciciosResueltos = 7, EquipoId = 1, MaratonId = 1},
                    new Participacion {Puesto = 2,NumeroEjerciciosResueltos = 5, EquipoId = 5, MaratonId = 1},
                    new Participacion {Puesto = 3,NumeroEjerciciosResueltos = 5, EquipoId = 6, MaratonId = 1},
                    new Participacion {Puesto = 4,NumeroEjerciciosResueltos = 3, EquipoId = 2, MaratonId = 1},
                    new Participacion {Puesto = 5,NumeroEjerciciosResueltos = 2, EquipoId = 7, MaratonId = 1},
                    new Participacion {Puesto = 6,NumeroEjerciciosResueltos = 1, EquipoId = 9, MaratonId = 1},
                    new Participacion {Puesto = 7,NumeroEjerciciosResueltos = 0, EquipoId = 8, MaratonId = 1},
                    new Participacion {Puesto = 1,NumeroEjerciciosResueltos = 4, EquipoId = 2, MaratonId = 2},
                    new Participacion {Puesto = 2,NumeroEjerciciosResueltos = 2, EquipoId = 1, MaratonId = 2},
                    new Participacion {Puesto = 3,NumeroEjerciciosResueltos = 1, EquipoId = 3, MaratonId = 2},
                    new Participacion {Puesto = 4,NumeroEjerciciosResueltos = 1, EquipoId = 4, MaratonId = 2}
                };
                participaciones.ForEach(e => ctx.Participacion.Add(e));
                ctx.SaveChanges();

                var patrocinadores = new List<Patrocinador>{
                    new Patrocinador { Nombre = "Eu Metus In Incorporated" },
                    new Patrocinador { Nombre = "Sed Associates" },
                    new Patrocinador { Nombre = "Quisque Limited" },
                    new Patrocinador { Nombre = "Interdum Libero PC" },
                    new Patrocinador { Nombre = "Magna Duis Dignissim Company" },
                    new Patrocinador { Nombre = "Lorem Corp" },
                    new Patrocinador { Nombre = "Elit Company" },
                    new Patrocinador { Nombre = "Duis Incorporated" },
                    new Patrocinador { Nombre = "Sit Amet Incorporated" },
                    new Patrocinador { Nombre = "Orci Company" }
                };
                patrocinadores.ForEach(e => ctx.Patrocinador.Add(e));
                ctx.SaveChanges();

                var contratos = new List<Contrato>
                {
                    new Contrato { MaratonId = 2, PatrocinadorId = 1 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 2 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 3 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 4 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 5 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 6 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 7 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 8 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 9 },
                    new Contrato { MaratonId = 2, PatrocinadorId = 10 },
                    new Contrato { MaratonId = 1, PatrocinadorId = 1 },
                    new Contrato { MaratonId = 1, PatrocinadorId = 2 },
                    new Contrato { MaratonId = 1, PatrocinadorId = 3 },
                    new Contrato { MaratonId = 1, PatrocinadorId = 4 },
                    new Contrato { MaratonId = 1, PatrocinadorId = 5 }
                };

                contratos.ForEach(e => ctx.Contrato.Add(e));
                ctx.SaveChanges();

                var ejercicios = new List<Ejercicio>
                {
                    new Ejercicio { Nombre = "Tarifa", Descripcion = "tarifaDescripcion.txt", EntradasSalidas = "" },
                    new Ejercicio { Nombre = "Take Two Stones", Descripcion = "takeTwoStonesDescripcion.txt", EntradasSalidas = "takeTwoStonesInputOuput.txt" },
                    new Ejercicio { Nombre = "Stuck In A Time Loop", Descripcion = "stuckInATimeLoopDescripcion.txt", EntradasSalidas = "stuckInATimeLoopInputOuput.txt" },
                    new Ejercicio { Nombre = "Solving for Carrots", Descripcion = "solvingforCarrotsDescripcion.txt", EntradasSalidas = "solvingforCarrotsInputOuput.txt" },
                    new Ejercicio { Nombre = "Sibice", Descripcion = "SibiceDescripcion.txt", EntradasSalidas = "SibiceInputOuput.txt" },
                    new Ejercicio { Nombre = "R2", Descripcion = "R2Descripcion.txt", EntradasSalidas = "R2InputOuput.txt" },
                    new Ejercicio { Nombre = "Quadrant Selection", Descripcion = "quadrantSelectionDescripcion.txt", EntradasSalidas = "quadrantSelectionInputOuput.txt" },
                    new Ejercicio { Nombre = "Pot", Descripcion = "PotDescripcion.txt", EntradasSalidas = "PotInputOuput.txt" },
                    new Ejercicio { Nombre = "Oddities", Descripcion = "OdditiesDescripcion.txt", EntradasSalidas = "OdditiesInputOuput.txt" },
                    new Ejercicio { Nombre = "Nasty Hacks", Descripcion = "nastyHacksDescripcion.txt", EntradasSalidas = "nastyHacksInputOuput.txt" },
                    new Ejercicio { Nombre = "Hissing Microphone", Descripcion = "hissingMicrophoneDescripcion.txt", EntradasSalidas = "hissingMicrophoneInputOuput.txt" },
                    new Ejercicio { Nombre = "Faktor", Descripcion = "FaktorDescripcion.txt", EntradasSalidas = "FaktorInputOuput.txt" },
                    new Ejercicio { Nombre = "Trik", Descripcion = "TrikDescripcion.txt", EntradasSalidas = "TrikInputOuput.txt" },
                    new Ejercicio { Nombre = "Star Arrangements", Descripcion = "starArrangementsDescripcion.txt", EntradasSalidas = "starArrangementsInputOuput.txt" }
                };
                ejercicios.ForEach(e => ctx.Ejercicio.Add(e));
                ctx.SaveChanges();

                var ejerciciosMaraton = new List<EjercicioMaraton>
                {
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 1 },
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 2 },
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 3 },
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 4 },
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 5 },
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 6 },
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 7 },
                    new EjercicioMaraton { MaratonId = 2, EjercicioId = 8 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 7 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 8 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 9 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 10 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 11 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 12 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 13 },
                    new EjercicioMaraton { MaratonId = 1, EjercicioId = 14 }
                };
                ejerciciosMaraton.ForEach(e => ctx.EjercicioMaraton.Add(e));
                ctx.SaveChanges();
            }
        }
    }
}
