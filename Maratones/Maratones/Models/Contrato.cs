﻿namespace Maratones.Models
{
    using System;
    using System.Collections.Generic;

    public partial class Contrato
    {
        public int Id { get; set; }
        public int MaratonId { get; set; }
        public int PatrocinadorId { get; set; }

        public virtual Maraton Maraton { get; set; }
        public virtual Patrocinador Patrocinador { get; set; }
    }
}