﻿namespace Maratones.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Ejercicio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ejercicio()
        {
            this.EjercicioMaraton = new HashSet<EjercicioMaraton>();
        }
        [Required(ErrorMessage = "Digite el nombre del Ejercicio"), MaxLength(50)]
        public string Nombre { get; set; }
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string EntradasSalidas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EjercicioMaraton> EjercicioMaraton { get; set; }
    }
}