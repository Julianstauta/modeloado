﻿namespace Maratones.Models
{
    using System;
    using System.Collections.Generic;

    public partial class EjercicioMaraton
    {
        public int Id { get; set; }
        public int MaratonId { get; set; }
        public int EjercicioId { get; set; }

        public virtual Maraton Maraton { get; set; }
        public virtual Ejercicio Ejercicio { get; set; }
    }
}