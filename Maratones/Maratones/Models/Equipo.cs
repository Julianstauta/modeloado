﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Maratones.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Equipo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Equipo()
        {
            this.Estudiante = new HashSet<Estudiante>();
            this.Participacion = new HashSet<Participacion>();
        }
        [Required(ErrorMessage = "Digite el nombre del Equipo"), MaxLength(50)]
        public string Nombre { get; set; }
        public int Id { get; set; }
        public string ProfesorCedula { get; set; }
        public string UniversidadNombre { get; set; }
        public int ProfesorId { get; set; }
        public int UniversidadId { get; set; }

        public virtual Profesor Profesor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estudiante> Estudiante { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Participacion> Participacion { get; set; }
    }
}
