﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Maratones.Models
{

    public partial class Estudiante
    {
        public string Cedula { get; set; }
        public int Id { get; set; }
        [Required(ErrorMessage = "Digite el nombre del Estudiante"), MaxLength(50)]
        public string Nombre { get; set; }
        public string CodigoEstudiante { get; set; }
        public string EquipoNombre { get; set; }
        public int UniversidadId { get; set; }
        public int EquipoId { get; set; }

        public virtual Universidad Universidad { get; set; }
        public virtual Equipo Equipo { get; set; }
    }
}
