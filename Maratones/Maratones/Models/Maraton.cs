﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Maratones.Models
{
    public partial class Maraton
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Maraton()
        {
            this.Participacion = new HashSet<Participacion>();
            this.Contrato = new HashSet<Contrato>();
            this.EjercicioMaraton = new HashSet<EjercicioMaraton>();
        }
        [Required(ErrorMessage = "Digite el nombre de la Maraton"), MaxLength(50)]
        public string NombreMaraton { get; set; }
        public int Id { get; set; }
        public string Ganador { get; set; }
        public string Premio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Participacion> Participacion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contrato> Contrato { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EjercicioMaraton> EjercicioMaraton { get; set; }
    }
}