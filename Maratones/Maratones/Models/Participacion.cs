﻿namespace Maratones.Models
{
    public partial class Participacion
    {
        public int Puesto { get; set; }
        public int NumeroEjerciciosResueltos { get; set; }
        public int Id { get; set; }
        public int EquipoId { get; set; }
        public int MaratonId { get; set; }

        public virtual Equipo Equipo { get; set; }
        public virtual Maraton Maraton { get; set; }
    }
}