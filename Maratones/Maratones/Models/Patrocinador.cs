﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Maratones.Models
{
    public partial class Patrocinador
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Patrocinador()
        {
            this.Contrato = new HashSet<Contrato>();
        }
        [Required(ErrorMessage = "Digite el nombre del Patrocinador"), MaxLength(50)]
        public string Nombre { get; set; }
        public int Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contrato> Contrato { get; set; }
    }
}