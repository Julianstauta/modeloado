﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Maratones.Models
{
    public partial class Profesor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Profesor()
        {
            this.Equipo = new HashSet<Equipo>();
        }

        public string Cedula { get; set; }
        [Required(ErrorMessage = "Digite el nombre del Profesor"), MaxLength(50)]
        public string Nombre { get; set; }
        public int Id { get; set; }
        public int UniversidadId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Equipo> Equipo { get; set; }
        public virtual Universidad Universidad { get; set; }
    }
}