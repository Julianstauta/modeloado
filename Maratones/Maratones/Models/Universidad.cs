﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Maratones.Models
{
    public partial class Universidad
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Universidad()
        {
            this.Profesor = new HashSet<Profesor>();
            this.Estudiante = new HashSet<Estudiante>();
        }

        public string Nombre { get; set; }
        public int Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Profesor> Profesor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estudiante> Estudiante { get; set; }
    }
}