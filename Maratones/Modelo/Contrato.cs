//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class Contrato
    {
        public int Id { get; set; }
        public int MaratonId { get; set; }
        public int PatrocinadorId { get; set; }
    
        public virtual Maraton Maraton { get; set; }
        public virtual Patrocinador Patrocinador { get; set; }
    }
}
