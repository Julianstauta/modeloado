//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class Estudiante
    {
        public string Cedula { get; set; }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string CodigoEstudiante { get; set; }
        public string EquipoNombre { get; set; }
        public int UniversidadId { get; set; }
        public int EquipoId { get; set; }
    
        public virtual Universidad Universidad { get; set; }
        public virtual Equipo Equipo { get; set; }
    }
}
